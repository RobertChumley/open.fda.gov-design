#Pool 1- Design

##Team Radus Software- Technical Approach
![alt tag](https://bytebucket.org/RobertChumley/open.fda.org-project/raw/eb79e64ea8ead29797bd960e9d5bc69c834ced2b/public/arrow-image.png)

[Link to working design prototype](http://fdacustompilot-designproto.azurewebsites.net/)

### Step 1. Establish Team
As a first step in designing and developing the working prototype, the Radus team identified the leader (Program Manager - Attachment B). The leader ensured that the project was fully controlled, that goals and objectives were set, program responsibility was assigned, and results are documented. The Program Manger then divided the group into a design team and a development team that were empowered to make local decisions and were accountable to deliver on the prototype needs.  Radus Software is submitting a response for Pool 2- Development. Both the teams worked in cadence and synchronized to the release schedule.

### Step 2. Develop Vision
Vision Creation: The team began by exploring the data sets provided and by creating the prototype vision of providing intelligent tools to guide the public on adverse events arising due to pharmaceutical drugs, medical devices and food. 

Identification of Strategic Themes: The next step was for the design team to study the data sets, use a range of qualitative and quantitative research methods to determine citizen needs from such data and to arrive at the strategic themes for product creation. These themes were specific, itemized business improvements to be realized in converting the as-is data set to invaluable information. The themes identified from the analysis of data were 

1.	Develop a tool to help the public identify adverse events due to specific pharmaceutical drugs.
2.	Develop a tool to help the public identify adverse events due to specific medical devices.
3.	Develop a tool to help the public identify adverse events due to recalled food items.

These themes were identified to improve the way the citizens are able to use the data from the publicly available FDA data site.

### Step 3. Agile Cycles 
The next step in our agile approach was the planning of the sprints for the design of this prototype.  In looking at our development timeline, we established the duration for the sprint cycles. Daily Scrums were conducted to help the team set the context for the coming days work.  This meeting was time-boxed to 15 minutes to keep the discussions concise and relevant.  The team could quickly share information on what was accomplished yesterday, what is planned for today, and report any impediments.  The Scrum Master was responsible for helping the team resolve impediments. 

At the end of each Sprint, we conducted Sprint Reviews to share lessons learned and provide input for continuous improvement.  The Sprint Review took place at the end of each three day sprint.  The Sprint Review started with a quick review of the sprint goals and metrics followed by an explanation of each completed story.  We also identified the business and architectural artifacts that we would produce for the development team.  The individual roles were broken down as follows:

IT Program manager (Appendix B: Category 1- Product Manager): project leader managed all sprints, team lead, and communications lead

Senior Business Analyst (Appendix B: Category 3- Interaction Designer/User): Researching of UX, Development of use cases and other architectural artifacts. Use cases were used to capture complex user-to-system or system-to-system interaction.  Split according to the specific scenarios or user goals of the use case.

Subject Matter Specialist (Appendix B: Category 4- Writer/Content): The Subject Matter Specialist was responsible for identifying the detailed data and requirement analysis and creation of related features and sub-feature sets. Features and Benefits Matrix (FAB) were used to describe each Feature.  The Features and Benefits Matrix comprised three columns.  The Feature, that used a short phrase to give the Feature a name and some implied context.  The Benefit, that provided a short description of each benefit of the Feature to the citizen and Acceptance Criteria, that would be used by the development team to determine that the feature had been implemented correctly.  

Subject matter Specialist and System Developer I (Appendix B: Categories 5- Visual Designer and 6- Front End Web Developer).  The team comprising the Visual Designer and Front end Web Developer were responsible for using modern techniques to develop storyboards and wireframes. 

Agile Architecture Principles were used to ensure that every Sprint participant saw the bigger picture.   The true agile idea of using the simplest architecture and design to support the need was used. The right balance of intentional architecture and emergent design allowed for generalization of solution elements that addressed common problems at the system level.

Copy the file Food Drug Consumer Tracking System - Design Prototype.PPT to the desired location and launch it by clicking on the file name